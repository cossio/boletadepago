const CalculadoraPorFijo = require("./Dominio/Entidades/calculadoraSalario/calculadoraPorFijo");
const CalculadoraPorHora = require("./Dominio/Entidades/calculadoraSalario/calculadoraPorHora");
const CalculadoraPorComision = require("./Dominio/Entidades/calculadoraSalario/calculadoraPorComision");
const CalculadoraDeFechaDePagoFijo = require("./Dominio/Entidades/calculadoraFechaDePago/calculadoraDeFechaDePagoFijo");
const CalculadoraDeFechaDePagoPorHora = require("./Dominio/Entidades/calculadoraFechaDePago/calculadoraDeFechaDePagoPorHora");
const CalculadoraDeFechaDePagoPorComision = require("./Dominio/Entidades/calculadoraFechaDePago/CalculadoraDeFechaDePagoPorComision");
const TarjetaAsistencia = require("./Dominio/Entidades/tarjetas/tarjetaAsistencia");
const TarjetaHora = require("./Dominio/Entidades/tarjetas/tarjetaHora");
const TarjetaVenta = require("./Dominio/Entidades/tarjetas/tarjetaVenta");
const Notificacion = require("./Dominio/Entidades/notificador/notificacion");
const SMS = require("./Dominio/Entidades/notificador/SMS");
const Whatsapp = require("./Dominio/Entidades/notificador/whatsapp");


class ConfiguracionPorDefecto {
    constructor() {
        this.notificador = new SMS(new Whatsapp(new Notificacion(""), ""), "");
    }

    obtenerConfiguracion(tipo) {
        switch (tipo) {
            case 'Fijo':
                return this.obtenerConfiguracionEmpleadoFijo();
            case 'Hora':
                return this.obtenerConfiguracionEmpleadoPorHora();
            case 'Comision':
                return this.obtenerConfiguracionEmpleadoPorComision();
            default:
                return "No existe el tipo";
        }
    }

    obtenerConfiguracionEmpleadoFijo() {
        let configuracionPorDefecto = {};
        let diasTrabajados = [new TarjetaAsistencia("2019-05-03"),
        new TarjetaAsistencia("2019-05-04")
        ];
        let calculadoraDeSalario = new CalculadoraPorFijo(2000, diasTrabajados);
        let calculadoraDeFechaDePago = new CalculadoraDeFechaDePagoFijo(new Date());
        configuracionPorDefecto = {
            calculadoraDeSalario: calculadoraDeSalario,
            calculadoraFechaPago: calculadoraDeFechaDePago,
            notificador: this.notificador
        };
        return configuracionPorDefecto;
    }

    obtenerConfiguracionEmpleadoPorHora() {
        let configuracionPorDefecto = {};
        let tarjetaHora1 = new TarjetaHora("2019-05-03", "08:00:00", "22:00:00");    
        let calculadora = new CalculadoraPorHora(100, [tarjetaHora1])
        let calculadoraDeFechaDePago = new CalculadoraDeFechaDePagoPorHora(new Date());
        configuracionPorDefecto = {
            calculadoraDeSalario: calculadora,
            calculadoraFechaPago: calculadoraDeFechaDePago,
            notificador: this.notificador
        };
        return configuracionPorDefecto;
    }

    obtenerConfiguracionEmpleadoPorComision() {
        let configuracionPorDefecto = {};
        let tarjetas = [new TarjetaVenta(600, "2019-05-03"),
        new TarjetaVenta(500, "2019-05-03"),
        new TarjetaVenta(400, "2019-05-04"),
        new TarjetaVenta(500, "2019-05-04")
        ];
        let calculadoraDeSalario = new CalculadoraPorComision(500, 0.12, tarjetas);
        let calculadoraDeFechaDePago = new CalculadoraDeFechaDePagoPorComision(new Date());
        configuracionPorDefecto = {
            calculadoraDeSalario: calculadoraDeSalario,
            calculadoraFechaPago: calculadoraDeFechaDePago,
            notificador: this.notificador
        };
        return configuracionPorDefecto;
    }

}

module.exports = ConfiguracionPorDefecto;