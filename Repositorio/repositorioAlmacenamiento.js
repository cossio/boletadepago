class RepositorioAlmacenamiento {
    constructor(bd){
        this.bd = bd;
    }


    registrarObjeto(objeto,nombreDeConjunto){
        return this.bd.registrarObjeto(objeto,nombreDeConjunto);
    }

    registrarObjetos(listaDeObjetos,nombreDeConjunto){
        listaDeObjetos.forEach( objeto => {
            this.registrarObjeto(objeto,nombreDeConjunto);
        });
    }

    obtenerListaDe(nombreDeConjunto){
        return this.bd.obtenerListaDe(nombreDeConjunto);
    }

    eliminarObjeto(id,nombreDeConjunto){
        return this.bd.eliminarObjeto(id,nombreDeConjunto);
    }

    obtenerPorCI(ci,nombreDeConjunto){
        return this.bd.obtenerPorCI(ci,nombreDeConjunto);
    }
}

module.exports = RepositorioAlmacenamiento;