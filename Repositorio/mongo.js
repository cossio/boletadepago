const MongoClient = require('mongodb').MongoClient;

class Mongo {
    constructor(enlace, nombreDeBD) {
        this.enlace = enlace;
        this.nombreBD = nombreDeBD;
    }

    async establecerConexion() {
        this.conexion = await MongoClient.connect(this.enlace, { useNewUrlParser: true });
        this.baseDeDatos = await this.conexion.db(this.nombreBD);
    }

    desconectarse() {
        this.conexion.close();
    }

    registrarObjeto(objeto, nombreDeConjunto) {
        let coleccion = this.baseDeDatos.collection(nombreDeConjunto);
        let seRegistro = coleccion.insertOne(objeto)
            .then(() => { return true; })
            .catch(() => { return false; });
        return seRegistro;
    }

    obtenerListaDe(nombreDeConjunto) {
        let coleccion = this.baseDeDatos.collection(nombreDeConjunto);
        let cursor = coleccion.find({});
        let arreglo = cursor.toArray();
        return arreglo;
    }

    eliminarObjeto(id, nombreDeConjunto) {
        let consulta = { _id: id };
        let coleccion = this.baseDeDatos.collection(nombreDeConjunto);
        return coleccion.deleteOne(consulta);
    }

    obtenerPorCI(ciBuscado, nombreDeConjunto) {
        let coleccion = this.baseDeDatos.collection(nombreDeConjunto);
        let consulta = { ci: ciBuscado };
        let cursor = coleccion.find(consulta);
        let lista = cursor.toArray();
        return lista;
    }

}

module.exports = Mongo;









