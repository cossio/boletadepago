const express = require('express');
const app = express();
const path = require('path');


let ModeloSolicitudRegistrarEmpleado = require('../../dominio/Dtos/modeloSolicitudRegistrarEmpleado');
let InteractorRegistrarEmpleado = require('../../dominio/interactores/interactorRegistrarEmpleado');
let PresentadorRegistrarEmpleado = require('../presentadores/presentadorRegistrarEmpleado');
let VistaRegistrarEmpleado = require('../vistas/vistaRegistrarEmpleado');


let InteractorObtenerEmpleados = require('../../dominio/interactores/interactorObtenerEmpleados');
let PresentadorObtenerEmpleados = require('../presentadores/presentadorObtenerEmpleados');
let VistaObtenerEmpleados = require('../vistas/vistaObtenerEmpleados');


const bodyParser = require('body-parser');
let VistaObtenerBoletas = require('../vistas/vistaObtenerBoletas');
let PresentadorObtenerBoletas = require('../presentadores/presentadorObtenerBoletas');
let IteractortObtenerBoletas = require('../../dominio/interactores/iteractorObtenerBoletas');
let ModeloSolicitudMostrarBoletaEmpleado= require('../../dominio/Dtos/modeloSolicitudMostrarBoletaEmpleado');

let ModeloSolicitudCrearBoletaEmpleado = require('../../Dominio/Dtos/modeloSolicitudCrearBoletaEmpleado');
let VistaCrearBoletaEmpleado = require("../vistas/vistaCrearBoletaEmpleado");
let PresentadorCrearBoletaEmpleado = require('../presentadores/presentadorCrearBoletaEmpleado');
let InteractorCrearBoletaEmpleado = require('../../dominio/interactores//interactorCrearBoletaEmpleado');

app.use(bodyParser.urlencoded({
    extended: true
}));

class ControladorEmpleado {
    constructor(repositorio, rutaNombre, configuracionInicial) {
        app.set('views', path.join(rutaNombre, 'views'));
        app.set('view engine', 'ejs');
        this.repositorio = repositorio;
        this.configuracionInicial = configuracionInicial;
    }
    async definirRutas() {
        app.get('/', async (req, res) => {
            let vista = new VistaObtenerEmpleados(res);
            let presentador = new PresentadorObtenerEmpleados(vista);

            let interactorObtenerEmpleados = new InteractorObtenerEmpleados();
            await interactorObtenerEmpleados.obtenerListaEmpleados(this.repositorio, presentador);
        });
        app.post('/add', async (req, res) => {
            let modeloSolicitud = new ModeloSolicitudRegistrarEmpleado();
            let modeloSolicitudRegistrarEmpleado = modeloSolicitud.obtenerModeloSolicitud(req);

            let vista = new VistaRegistrarEmpleado(res);
            let presentador = new PresentadorRegistrarEmpleado(vista);

            let interactorRegistrarEmpleado = new InteractorRegistrarEmpleado();
            await interactorRegistrarEmpleado.registrarEmpleado(modeloSolicitudRegistrarEmpleado, this.repositorio, presentador);

        });
        
        app.get('/crear-boleta/:tipo/:ci', async (req, res) => {
            let modeloSolicitud = new ModeloSolicitudCrearBoletaEmpleado(this.configuracionInicial);
            let modeloSolicitudGenerarBoletaEmpleado = modeloSolicitud.obtenerModeloSolicitud(req);

            let vista = new VistaCrearBoletaEmpleado(res);
            let presentador = new PresentadorCrearBoletaEmpleado(vista);

            let interactorCrearBoletaEmpleado = new InteractorCrearBoletaEmpleado();
            await interactorCrearBoletaEmpleado.crearBoletaEmpleado(modeloSolicitudGenerarBoletaEmpleado, this.repositorio, presentador);
        });
        
        app.get('/mostrarBoletas/:ci',async (req,res)=>{
            let modeloSolicitud=new ModeloSolicitudMostrarBoletaEmpleado();
            let modeloSolicitudMostrarBoletaEmpleado = modeloSolicitud.obtenerModeloSolicitud(req);            
            
            let vista  = new VistaObtenerBoletas(res);
            let presentador = new PresentadorObtenerBoletas(vista);

            let interactorObtenerBoletas = new IteractortObtenerBoletas();
            await interactorObtenerBoletas.obtenerBoletas(modeloSolicitudMostrarBoletaEmpleado,this.repositorio,presentador);

        });


    }
    iniciarConeccion(puerto) {
        app.listen(puerto, () => {
            console.log("Server on port: " + puerto);
        });
    }
}
module.exports = ControladorEmpleado;

