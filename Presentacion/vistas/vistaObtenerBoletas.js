class VistaObtenerBoletas{
    constructor(res){
        this.res=res;
    }
    render(){
        
        this.res.render('mostrarBoletas',{
            boletas: this.boletas
        });
    }
    mostrarMensajeDeRegistroExitoso(modeloVistaRegistrarEmpleado){
        this.boletas=modeloVistaRegistrarEmpleado; 
    }
}
module.exports=VistaObtenerBoletas;