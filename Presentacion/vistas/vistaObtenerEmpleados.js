class VistaObtenerEmpleados{
    constructor(res){
        this.res=res;
    }
    render(){
        
        this.res.render('index',{
            empleados: this.empleados
        });
    }
    mostrarMensajeDeRegistroExitoso(modeloVistaRegistrarEmpleado){
        this.empleados=modeloVistaRegistrarEmpleado; 
    }
}
module.exports=VistaObtenerEmpleados;