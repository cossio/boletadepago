class VistaCrearBoletaEmpleado{
    constructor(res){
        this.res=res;
    }
    render(){
        this.res.redirect("/");
    }
    mostrarMensajeDeRespuesta(modeloVistaRegistrarEmpleado){
        this.mensaje=modeloVistaRegistrarEmpleado; 
    }
}
module.exports=VistaCrearBoletaEmpleado;