class VistaRegistrarEmpleado{
    constructor(res){
        this.res=res;
    }
    render(){
        this.res.redirect("/");
    }
    mostrarMensajeDeRegistroExitoso(modeloVistaRegistrarEmpleado){
        this.mensaje=modeloVistaRegistrarEmpleado.descripcion; 
    }
}
module.exports=VistaRegistrarEmpleado;