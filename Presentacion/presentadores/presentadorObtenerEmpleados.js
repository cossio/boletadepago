let ModeloVistaObtenerEmpleados = require('../../presentacion/modelosVista/modeloVistaObtenerEmpleados');
class PresentadorObtenerEmpleados{
    constructor(vista){
        this.vista=vista;
       
    }
    modelarRespuesta(modeloRespuestaObtenerEmpleados){
        
        let modeloVista = new ModeloVistaObtenerEmpleados();
        let modeloVistaRegistrarEmpleado = modeloVista.obtenerModeloVista(modeloRespuestaObtenerEmpleados);
        this.vista.mostrarMensajeDeRegistroExitoso(modeloVistaRegistrarEmpleado);
        this.vista.render();
    }

}
module.exports=PresentadorObtenerEmpleados;