const ModeloVistaCrearBoletaEmpleado = require('../../presentacion/modelosVista/modeloVistaCrearBoletaEmpleado');

class PresentadorCrearBoletaEmpleado{
    constructor(vista){
        this.vista=vista;
       
    }
    modelarRespuesta(modeloRespuestaCrearBoletaEmpleado){
        let modeloVista = new ModeloVistaCrearBoletaEmpleado();
        let modeloVistaCrearBoletaEmpleado = modeloVista.obtenerModeloVista(modeloRespuestaCrearBoletaEmpleado);
        this.vista.mostrarMensajeDeRespuesta(modeloVistaCrearBoletaEmpleado);
        this.vista.render();
    }

}
module.exports=PresentadorCrearBoletaEmpleado;