let modeloVistaObtenerBoletas = require('../../presentacion/modelosVista/modeloVistaObtenerBoletas');
class PresentadorObtenerEmpleados{
    constructor(vista){
        this.vista=vista;
       
    }
    modelarRespuesta(modeloRespuestaObtenerBoletas){
        
        let modeloVista = new modeloVistaObtenerBoletas();
        let modeloVistaRegistrarBoleta = modeloVista.obtenerModeloVista(modeloRespuestaObtenerBoletas);
        this.vista.mostrarMensajeDeRegistroExitoso(modeloVistaRegistrarBoleta);
        this.vista.render();
    }

}
module.exports=PresentadorObtenerEmpleados;