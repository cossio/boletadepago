let ModeloVistaRegistrarEmpleado = require('../../presentacion/modelosVista/modeloVistaRegistrarEmpleado');
class PresentadorRegistrarEmpleado{
    constructor(vista){
        this.vista=vista;
       
    }
    modelarRespuesta(modeloRespuestaRegistrarEmpleado){
        
        let modeloVista = new ModeloVistaRegistrarEmpleado();
        let modeloVistaRegistrarEmpleado = modeloVista.obtenerModeloVista(modeloRespuestaRegistrarEmpleado);
        this.vista.mostrarMensajeDeRegistroExitoso(modeloVistaRegistrarEmpleado);
        this.vista.render();
    }

}
module.exports=PresentadorRegistrarEmpleado;