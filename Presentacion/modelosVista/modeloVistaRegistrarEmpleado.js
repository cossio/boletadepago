class ModeloVistaRegistrarEmpleado{
    constructor(){

    }
    obtenerModeloVista(modeloRespuestaRegistrarEmpleado){
        if(modeloRespuestaRegistrarEmpleado.descripcion=="Exito"){
            let mensaje = {
                descripcion: "Empleado registrado exitosamente"
            }
            return mensaje;
        }
        else{
            let mensaje = {
                descripcion: "Hubo un error al registrar el empleado"
            }
            return mensaje;
        }
    }
}

module.exports=ModeloVistaRegistrarEmpleado;