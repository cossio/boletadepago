const ControladorEmpleado = require('./presentacion/controladores/controladorEmpleado');
const Mongo = require('./repositorio/mongo');

const ConfiguracionPorDefecto = require("./configuracionPorDefecto");

var mongo = new Mongo('mongodb://localhost/27017', 'BoletasDePagoBD');
var mongoConected = mongo.establecerConexion()


mongoConected.then(() => {
    let configuracionPorDefecto = new ConfiguracionPorDefecto();
    let controladorEmpleado = new ControladorEmpleado(mongo, __dirname, configuracionPorDefecto);
    controladorEmpleado.iniciarConeccion(3000);
    controladorEmpleado.definirRutas();
})
