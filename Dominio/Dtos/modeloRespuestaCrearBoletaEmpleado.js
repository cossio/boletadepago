class ModeloRespuestaCrearBoletaEmpleado {
    constructor() {
    }

    obtenerModeloRespuesta(estaBoletaCreada) {
        let mensaje={descripcion:"Boleta no creada"};
        if (estaBoletaCreada) {
            mensaje={descripcion:"Boleta creada"};
        }
        return mensaje;
    }
}

module.exports = ModeloRespuestaCrearBoletaEmpleado;
