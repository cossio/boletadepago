var Empleado = require('../entidades/empleado/empleado.js');

class ModeloSolicitudCrearBoletaEmpleado {
    constructor(configuracionInicial) {
        this.configuracionInicial = configuracionInicial;
    }


    obtenerModeloSolicitud(peticion) {
        let modeloSolicitud;
        let datosPeticion = this.obtenerParametrosSolicitud(peticion);
        let configuracionDeModeloPorDefecto = this.configuracionInicial.obtenerConfiguracion(datosPeticion.tipo);
        let empleado = new Empleado(
            "",
            "",
            configuracionDeModeloPorDefecto.calculadoraDeSalario,
            configuracionDeModeloPorDefecto.calculadoraFechaPago,
            "",
            configuracionDeModeloPorDefecto.notificador
        );
        modeloSolicitud = { empleado, ci: datosPeticion.empleadoCI }
        return modeloSolicitud;
    }

    obtenerParametrosSolicitud(peticion) {
        let datosPeticion = {
            empleadoCI: peticion.params.ci,
            tipo: peticion.params.tipo
        }
        return datosPeticion;
    }
}

module.exports = ModeloSolicitudCrearBoletaEmpleado;