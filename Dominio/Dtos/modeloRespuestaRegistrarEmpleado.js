class ModeloRespuestaRegistrarEmpleado { 
    constructor(){
    }

    obtenerModeloRespuesta(estaRegistrado){
        if(estaRegistrado==true){
            let mensaje = {
                descripcion: "Exito"
            }
            return mensaje;
        }
        else{
            let mensaje = {
                descripcion: "Error"
            }
            return mensaje;
        }
    }
}

module.exports = ModeloRespuestaRegistrarEmpleado;
