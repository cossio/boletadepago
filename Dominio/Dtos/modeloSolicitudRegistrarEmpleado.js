class ModeloSolicitudRegistrarEmpleado { 
    constructor(){
    }

    obtenerModeloSolicitud(peticion){
        let cuerpoPeticion = peticion.body;
        var empleado = {
            ci:cuerpoPeticion.ci,
            nombre:cuerpoPeticion.nombre,
            tipo:cuerpoPeticion.tipo,
            metodoDePago:cuerpoPeticion.metodoDePago,
            salario:cuerpoPeticion.salario,
            celular:cuerpoPeticion.celular,
            tarjetas:[],
            boletas:[]
        };
        return empleado;
    }
}

module.exports = ModeloSolicitudRegistrarEmpleado;