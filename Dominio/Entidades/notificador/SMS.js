class SMS{
    constructor(notificador,destinatario) {
        this.notificador = notificador;
        this.destinatario=destinatario;

        this.accountSid = 'AC66ca54dba092d5e97a5ba8349b2ab74f';
        this.authToken = "1fe72660c2f7ea99731809752dad0d7b";
        this.conexion = this.iniciarConexion();
        this.opcionesDeEnvio = this.crearMensaje();
    }

    iniciarConexion() {
        return this.conexion = require('twilio')(this.accountSid, this.authToken);
    }
    crearMensaje() {
        return this.opcionesDeEnvio = {
            from: '+12672042065',
            body: " Eric: Alavaros has los test de la BD, el obtener y eliminar. Sino penitencia con zukulencia mañana si o si :v",
            to: this.destinatario
        }
    }

    async enviarNotificacion(){
        let a= await this.notificador.enviarNotificacion();
        console.log("Gmail: ",a);
        return await this.enviarNotificacionPorSMS();
    }

    async enviarNotificacionPorSMS() {
        let estado;
        estado = await this.conexion.messages.create(this.opcionesDeEnvio)
            .then(function () {
                return "mensaje enviado";
            })
            .catch(function () {
                console.log("SMS error");
                return "Error de envio de SMS";
            });
        return estado;
    }

}

module.exports=SMS;