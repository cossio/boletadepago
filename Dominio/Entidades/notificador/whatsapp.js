class Whatsapp{
    constructor(notificador,destinatario) {
        this.notificador = notificador;
        this.destinatario=destinatario;

        this.accountSid = 'AC66ca54dba092d5e97a5ba8349b2ab74f';
        this.authToken = '1fe72660c2f7ea99731809752dad0d7b';
        this.conexion = this.iniciarConexion();
        this.opcionesDeEnvio = this.crearMensaje();
    }

    iniciarConexion() {
        return this.conexion = require('twilio')(this.accountSid, this.authToken);
    }
    crearMensaje() {
        return this.opcionesDeEnvio = {
            from: 'whatsapp:+14155238886',
            body: "Mensaje para Alvaros",
            to: 'whatsapp:' + this.destinatario
        }
    }

    async enviarNotificacion(){
        await this.notificador.enviarNotificacion();
        return await this.enviarNotificacionPorWhatsapp();
    }

    async enviarNotificacionPorWhatsapp() {
        let estado;
        estado = await this.conexion.messages.create(this.opcionesDeEnvio)
            .then(function () {
                return "mensaje enviado";
            })
            .catch(function () {
                return "Error de envio de WhatsApp";
            });
        return estado;
    }

}

module.exports=Whatsapp;