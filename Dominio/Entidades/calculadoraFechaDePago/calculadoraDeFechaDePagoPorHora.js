class CalculadoraDeFechaDePagoPorHora{

    constructor(fechaDeInicioLaboral){
        this.fechaDeInicioLaboral = fechaDeInicioLaboral;
    }
    calcularFechaDePago(){
        let fechaDePago=this.fechaDeInicioLaboral;

        while(fechaDePago.getDay()!= 5){

            fechaDePago.setDate(fechaDePago.getDate()+1);
        }
        return fechaDePago;        

    }
    correspondePagar(fechaActual){
        return this.calcularFechaDePago().getTime()==fechaActual.getTime();
    }

}

module.exports=CalculadoraDeFechaDePagoPorHora;
