import BoletaDePago from '../boleta/boletaDePago';
class GeneradorBoletasDePago{
    constructor(empleados){
        this.empleados=empleados;
        this.boletasGeneradas=[];
    }
   
    generarBoletasDePagoParaTodosLosEmpleados(){
        for (let empleado of this.empleados) {
            let boletaDePago = new BoletaDePago();
            if(this.correspondePagar(empleado)){
                boletaDePago = boletaDePago.generarBoleta(empleado);
                this.boletasGeneradas.push(boletaDePago);
            }          
        }
        
    }

    correspondePagar(empleado){
        return empleado.correspondePagar();
    }
}


module.exports=GeneradorBoletasDePago;