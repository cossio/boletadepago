class BoletaDePago{
    constructor(){
        
    }
   
    generarBoleta(empleado){
        let boleta={
            "ci":empleado.obtenerCi(),
            "empleado":empleado.obtenerNombre(),
            "salario": empleado.obtenerSalario(),
            "moneda": "Bs",
            "metodoDePago": empleado.obtenerMetodoPago(),
            "FechaDePago":empleado.obtenerFechaPago()
        };
        return boleta;
    }
}


module.exports=BoletaDePago;