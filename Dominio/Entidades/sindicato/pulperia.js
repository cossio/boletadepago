class Pulperia {
    constructor(servicio) {
        this.listaDePullperia = [];
        this.servicio = servicio;
    }

    calcularMontoDeServiciosDelSindicato() {
        return this.servicio.calcularMontoDeServiciosDelSindicato() + this.calcularMontoDeLaLista();
    }

    agregarPedido(pedido, monto) {
        this.listaDePullperia.push({ pedido, monto });
    }

    calcularMontoDeLaLista() {
        let totalMonto = 0;
        for (let pedido of this.listaDePullperia) {
            totalMonto += pedido.monto;
        }
        return totalMonto;
    }



}

module.exports = Pulperia;