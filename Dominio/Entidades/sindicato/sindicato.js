class Sindicato{
    constructor(pagoMensual){
        this.pagoMensual=pagoMensual;
    }

    calcularMontoDeServiciosDelSindicato(){
        return this.pagoMensual;
    }

}

module.exports=Sindicato;