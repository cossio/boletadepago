class Prestamo {
    constructor(servicio) {
        this.servicio = servicio;
        this.montoAcumulado = [];
    }

    calcularMontoDeServiciosDelSindicato() {
        return this.servicio.calcularMontoDeServiciosDelSindicato() + this.calcularMontoDelPrestamo();
    }

    agregarPrestamo(monto, fecha) {
        this.montoAcumulado.push({ monto, fecha });
    }

    calcularMontoDelPrestamo() {
        let totalMonto = 0;
        for (let prestamo of this.montoAcumulado) {
            totalMonto += prestamo.monto;
        }
        return totalMonto;
    }

}

module.exports = Prestamo;