class Empleado{
    constructor(nombre,ci,calculadoraDeSalario,calculadoraFechaPago,metodoPago,notificador){
        this.nombre=nombre;
        this.ci=ci;
        this.calculadoraDeSalario=calculadoraDeSalario;
        this.calculadoraFechaPago=calculadoraFechaPago;
        this.metodoPago=metodoPago;
        this.notificador=notificador;
    }
    obtenerSalario(){
        return this.calculadoraDeSalario.calcularSalario();
    }
    obtenerFechaPago(){
        return this.calculadoraFechaPago.calcularFechaDePago();
    }
    correspondePagar(){
        return this.calculadoraFechaPago.correspondePagar(new Date());
    }
    notificar(){
        this.notificador.enviarNotificacion();
    }
    obtenerPago(){
        this.metodoPago.obtenerPago();
    }
    obtenerNombre(){
        return this.nombre;
    }
    obtenerCi(){
        return this.ci;
    }
    obtenerMetodoPago(){
        return this.metodoPago;
    }
    agregarServicioSindicato(servicio){
        this.servicio=servicio;
    }
    calcularMontoDeServiciosDelSindicato(){
        return this.servicio.calcularMontoDeServiciosDelSindicato();
    }
}


module.exports=Empleado;
