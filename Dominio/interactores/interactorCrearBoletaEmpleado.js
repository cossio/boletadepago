const ModeloRespuestaCrearBoletaEmpleado = require("../Dtos/modeloRespuestaCrearBoletaEmpleado");
const BoletaDePago = require("../Entidades/boleta/boletaDePago");


class InteractorCrearBoletaEmpleado {
    constructor() {
    }

    async crearBoletaEmpleado(solicitud, repositorio, presentador) {
        let empleadosBuscados = await repositorio.obtenerPorCI(solicitud.ci, 'Empleados');
        let empleadoEncotrado = empleadosBuscados[0];

        let boleta = this.crearBoleta(empleadoEncotrado, solicitud.empleado);
        let estaRegistrado = await repositorio.registrarObjeto(boleta, 'Boletas de Pago');

        let modeloRespuesta = new ModeloRespuestaCrearBoletaEmpleado();
        let modeloRespuestaCrearBoletaEmpleado = modeloRespuesta.obtenerModeloRespuesta(estaRegistrado);
        presentador.modelarRespuesta(modeloRespuestaCrearBoletaEmpleado);
    }

    crearBoleta(empleadoEncotrado, empleado) {
        let boleta = new BoletaDePago();
        empleado.ci = empleadoEncotrado.ci;
        empleado.nombre = empleadoEncotrado.nombre;
        empleado.calculadoraDeSalario.establecerSalario(parseInt(empleadoEncotrado.salario));
        empleado.metodoPago = empleadoEncotrado.metodoDePago;
        return boleta.generarBoleta(empleado);
    }

}

module.exports = InteractorCrearBoletaEmpleado;