let ModeloRespuestaRegistrarEmpleado = require('../../dominio/Dtos/modeloRespuestaRegistrarEmpleado');
class InteractorRegistrarEmpleado{
    constructor(){
    }

    async registrarEmpleado(empleado, repositorio,presentador) {
        let estaRegistrado=await repositorio.registrarObjeto(empleado,'Empleados');
        let modeloRespuesta = new ModeloRespuestaRegistrarEmpleado();
        let modeloRespuestaRegistrarEmpleado = modeloRespuesta.obtenerModeloRespuesta(estaRegistrado);
        presentador.modelarRespuesta(modeloRespuestaRegistrarEmpleado);
    }

}

module.exports=InteractorRegistrarEmpleado;