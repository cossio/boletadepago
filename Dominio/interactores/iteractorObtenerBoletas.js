let ModeloRespuestaObtenerBoletas = require('../../dominio/Dtos/modeloRespuestaObtenerBoletas');
class InteractorRegistrarBoletas{
    constructor(){
    }

    async obtenerBoletas(solicitud,repositorio,presentador) {
        let boletas = await repositorio.obtenerPorCI(solicitud.ci,'Boletas de Pago');
        let modeloRespuesta = new ModeloRespuestaObtenerBoletas();
        let modeloRespuestaObtenerBoletas = modeloRespuesta.obtenerModeloRespuesta(boletas);
        presentador.modelarRespuesta(modeloRespuestaObtenerBoletas);
    }

}

module.exports=InteractorRegistrarBoletas;