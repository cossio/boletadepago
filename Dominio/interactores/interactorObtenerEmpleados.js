let ModeloRespuestaObtenerEmpleados = require('../../dominio/Dtos/modeloRespuestaObtenerEmpleados');
class InteractorRegistrarEmpleado{
    constructor(){
    }

    async obtenerListaEmpleados(repositorio,presentador) {
        let empleados = await repositorio.obtenerListaDe('Empleados');
        let modeloRespuesta = new ModeloRespuestaObtenerEmpleados();
        let modeloRespuestaObtenerEmpleados = modeloRespuesta.obtenerModeloRespuesta(empleados);
        presentador.modelarRespuesta(modeloRespuestaObtenerEmpleados);
    }

}

module.exports=InteractorRegistrarEmpleado;