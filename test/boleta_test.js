var expect = require('chai').expect;

import Empleado from '../dominio/entidades/empleado/empleado.js';
import CalculadoraPorFijo from '../dominio/entidades/calculadoraSalario/calculadoraPorFijo';
import CalculadoraPorHora from '../dominio/entidades/calculadoraSalario/calculadoraPorHora';
import CalculadoraPorComision from '../dominio/entidades/calculadoraSalario/calculadoraPorComision';
import TarjetaHora from '../dominio/entidades/tarjetas/tarjetaHora';
import TarjetaVenta from '../dominio/entidades/tarjetas/tarjetaVenta';
import TarjetaAsistencia from '../dominio/entidades/tarjetas/tarjetaAsistencia';
import CalculadoraDeFechaDePagoPorHora from '../dominio/entidades/calculadoraFechaDePago/calculadoraDeFechaDePagoPorHora';
import CalculadoraDeFechaDePagoFijo from '../dominio/entidades/calculadoraFechaDePago/calculadoraDeFechaDePagoFijo';
import CalculadoraDeFechaDePagoPorComision from '../dominio/entidades/calculadoraFechaDePago/CalculadoraDeFechaDePagoPorComision';
import BoletaDePago from '../dominio/entidades/boleta/boletaDePago';

function compararObjetos(objeto1, objeto2) {
    if (JSON.stringify(objeto1) === JSON.stringify(objeto2)) {
        return true;
    }
    return false;
}

describe('boleta de pago', function () {
    it('recibe un empleado fijo y genera su boleta de pago', function () {
        let tarjetaAsistencia = new TarjetaAsistencia("2019-03-02");
        let calculadora = new CalculadoraPorFijo(1800, [tarjetaAsistencia]);

        let fechaIncioLaboral = new Date(2019, 3, 2);
        let calculadoraDeFecha = new CalculadoraDeFechaDePagoFijo(fechaIncioLaboral);
        let empleado = new Empleado("Erick", 1, calculadora, calculadoraDeFecha, "Deposito");
        let boletaPago = new BoletaDePago();
        let fechaDePago = new Date(2019, 3, 30);
        fechaDePago.toString();
        let boletaEsperada = {
            "ci": 1,
            "empleado": "Erick",
            "salario": 90,
            "moneda": "Bs",
            "metodoDePago": "Deposito",
            "FechaDePago": fechaDePago
        };
        let boletaResultante = boletaPago.generarBoleta(empleado);
        expect(compararObjetos(boletaEsperada, boletaResultante)).equal(true);
    });
    
    it('recibe un empleado por hora y genera su boleta de pago', function () {
        let tarjetaHora1 = new TarjetaHora("2019-03-02", "08:00:00", "12:00:00");
        let calculadora = new CalculadoraPorHora(200, [tarjetaHora1]);
        expect(calculadora.calcularSalario()).equal(800);
        let fechaIncioLaboral = new Date(2019, 3, 2);
        let calculadoraDeFecha = new CalculadoraDeFechaDePagoPorHora(fechaIncioLaboral);
        let empleado = new Empleado("Erick", 1, calculadora, calculadoraDeFecha, "Efectivo");
        let boletaPago = new BoletaDePago();
        let fechaDePago = new Date(2019, 3, 5);
        fechaDePago.toString();
        let boletaEsperada = {
                                "ci": 1,
                                "empleado": "Erick",
                                "salario": 800,
                                "moneda": "Bs",
                                "metodoDePago": "Efectivo",
                                "FechaDePago": fechaDePago
                            };
        let boletaResultante = boletaPago.generarBoleta(empleado);
        expect(compararObjetos(boletaResultante,boletaEsperada)).equal(true);
    });

    it('recibe un empleado por comision y genera su boleta de pago', function () {
        let tarjetaVenta = new TarjetaVenta(500, "2018-03-02");
        let calculadora = new CalculadoraPorComision(200, 0.05, [tarjetaVenta]);

        let fechaIncioLaboral = new Date(2019, 3, 2);
        let calculadoraDeFecha = new CalculadoraDeFechaDePagoPorComision(fechaIncioLaboral);
        let empleado = new Empleado("Erick", 1, calculadora, calculadoraDeFecha, "Cheque");
        let boletaPago = new BoletaDePago();
        let fechaDePago = new Date(2019, 3, 12);
        fechaDePago.toString();
        let boletaEsperada = {
                                "ci": 1,
                                "empleado": "Erick",
                                "salario": 225,
                                "moneda": "Bs",
                                "metodoDePago": "Cheque",
                                "FechaDePago": fechaDePago
                            };
        let boletaResultante = boletaPago.generarBoleta(empleado);
        expect(compararObjetos(boletaResultante,boletaEsperada)).equal(true);
    });
});