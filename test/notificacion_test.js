var expect = require('chai').expect
import Notificacion from '../dominio/entidades/notificador/notificacion';
import Whatsapp from '../dominio/entidades/notificador/whatsapp';
import SMS from '../dominio/entidades/notificador/SMS';

describe('notificaciones', function () {

    it('Notificacion para Gmail', async function () {
        this.timeout(50000);

        let email = 'edwinatahuichi92@gmail.com';
        let correo=new Notificacion(email);
        let respuesta= await correo.enviarNotificacion()
        expect(respuesta).equal("mensaje enviado");
    });


    it('Notificacion con Gmail y Whatsapp',async  function () {
        this.timeout(50000);
        let correo = 'edwinatahuichi92@gmail.com';
        let celular = '+591 74320193';

        let whatsapp=new Whatsapp( new Notificacion(correo),celular);
        let respuesta= await whatsapp.enviarNotificacion()
        expect(respuesta).equal("mensaje enviado");
    });

    it('Notificacion con Gmail, Whatsapp y SMS',async  function () {
        this.timeout(50000);
        let correo = 'edwinatahuichi92@gmail.com';
        let celular = '+591 74320193';

        let sms=new SMS( new Whatsapp( new Notificacion(correo),celular),celular);
        let respuesta= await sms.enviarNotificacion()
        expect(respuesta).equal("mensaje enviado");
    });

});