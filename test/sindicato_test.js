var expect = require('chai').expect
import Sindicato from '../dominio/entidades/sindicato/sindicato';
import Prestamos from '../dominio/entidades/sindicato/prestamos';
import Pulperia from '../dominio/entidades/sindicato/pulperia';
import Empleado from "../Dominio/Entidades/empleado/empleado";

describe('Sindicato', function () {
    it('calcular pago del sindicato', function () {
        let sindicato = new Sindicato(50);
        expect(sindicato.calcularMontoDeServiciosDelSindicato()).equal(50);
    });

    it('calcular pago del sindicato con Servicio de Prestamo', function () {
        let sindicato = new Sindicato(50);
        let prestamo = new Prestamos(sindicato);
        prestamo.agregarPrestamo(100, new Date());
        prestamo.agregarPrestamo(300, new Date());
        expect(prestamo.calcularMontoDeServiciosDelSindicato()).equal(450);
    });

    it('calcular pago del sindicato con Servicio de Pulperia', function () {
        let sindicato = new Sindicato(50);
        let pulperia = new Pulperia(sindicato);
        pulperia.agregarPedido("Carne", 200);
        pulperia.agregarPedido("Verduras", 80);
        pulperia.agregarPedido("Carbon", 50);
        pulperia.agregarPedido("Chorizo", 60);
        pulperia.agregarPedido("Pan Marraqueta", 30);
        expect(pulperia.calcularMontoDeServiciosDelSindicato()).equal(470);
    });

    it('calcular pago del sindicato con Servicio de Prestamo y Pulperia', function () {
        let sindicato = new Sindicato(50);
        let prestamo = new Prestamos(sindicato);
        prestamo.agregarPrestamo(100, new Date());
        prestamo.agregarPrestamo(300, new Date());
        let pulperia = new Pulperia(prestamo);
        pulperia.agregarPedido("Carne", 200);
        pulperia.agregarPedido("Verduras", 80);
        pulperia.agregarPedido("Carbon", 50);
        pulperia.agregarPedido("Chorizo", 60);
        pulperia.agregarPedido("Pan Marraqueta", 30);

        expect(pulperia.calcularMontoDeServiciosDelSindicato()).equal(870);
    });

    it('calcular pago del sindicato con monto 0', function () {
        let sindicato = new Pulperia(new Prestamos(new Sindicato(0)));
        expect(sindicato.calcularMontoDeServiciosDelSindicato()).equal(0);
    });

    it('calcular el monto de los servicios del Sindicato para un Empleado', function () {
        let servicio = new Sindicato(50);
        servicio = new Prestamos(servicio);
        servicio.agregarPrestamo(100, new Date());
        servicio.agregarPrestamo(300, new Date());
        servicio = new Pulperia(servicio);
        servicio.agregarPedido("Carne", 200);
        servicio.agregarPedido("Verduras", 80);
        servicio.agregarPedido("Carbon", 50);
        servicio.agregarPedido("Chorizo", 60);
        servicio.agregarPedido("Pan Marraqueta", 30);

        let empleado=new Empleado("erick","1","","","","");
        empleado.agregarServicioSindicato(servicio);

        expect(empleado.calcularMontoDeServiciosDelSindicato()).equal(870);
    });
});