var expect = require('chai').expect
import CalculadoraDeFechaDePagoPorHora from '../dominio/entidades/calculadoraFechaDePago/calculadoraDeFechaDePagoPorHora';
import CalculadoraDeFechaDePagoFijo from '../dominio/entidades/calculadoraFechaDePago/calculadoraDeFechaDePagoFijo';
import CalculadoraDeFechaDePagoPorComision from '../dominio/entidades/calculadoraFechaDePago/CalculadoraDeFechaDePagoPorComision';

describe('CalculadoraFechaDePago', function () {
it('recibe una fecha y devuelve la fecha del viernes para pagar a un empleado por hora', function () {

    let fechaIncioLaboral = new Date(2019, 3, 8);
    let calculadoraDeFecha = new CalculadoraDeFechaDePagoPorHora(fechaIncioLaboral);
    let fechaResultante = calculadoraDeFecha.calcularFechaDePago();
    let fechaEsperada = new Date(2019, 3, 12);
    let dia = fechaResultante.getDay();
    let mes = fechaResultante.getMonth();
    let anio = fechaResultante.getFullYear();

    let diaEsperado = fechaEsperada.getDay();
    let mesEsperado = fechaEsperada.getMonth();
    let anioEsperado = fechaEsperada.getFullYear();

    expect(dia).equal(diaEsperado);
    expect(mes).equal(mesEsperado);
    expect(anio).equal(anioEsperado);

});

it('recibe una fecha y devuelve la fecha del ultimo dia habil del mes para un empleado fijo', function () {

    let fechaIncioLaboral = new Date(2019, 5, 3);
    let calculadoraDeFecha = new CalculadoraDeFechaDePagoFijo(fechaIncioLaboral);
    let fechaResultante = calculadoraDeFecha.calcularFechaDePago();
    let fechaEsperada = new Date(2019, 5, 28);
    let dia = fechaResultante.getDate();
    let mes = fechaResultante.getMonth();
    let anio = fechaResultante.getFullYear();

    let diaEsperado = fechaEsperada.getDate();
    let mesEsperado = fechaEsperada.getMonth();
    let anioEsperado = fechaEsperada.getFullYear();

    expect(dia).equal(diaEsperado);
    expect(mes).equal(mesEsperado);
    expect(anio).equal(anioEsperado);

});
it('recibe una fecha y devuelve la fecha del 2do viernes para pagar a un empleado por comision', function () {

    let fechaIncioLaboral = new Date(2019, 3, 8);
    let comprabanteDeFechaHora = new CalculadoraDeFechaDePagoPorComision(fechaIncioLaboral);
    let fechaResultante = comprabanteDeFechaHora.calcularFechaDePago();
    let day = fechaResultante.getDate();
    let month = fechaResultante.getMonth();
    let year = fechaResultante.getFullYear();

    let fechaEsperada = new Date(2019, 3, 19);
    let diaEsperado = fechaEsperada.getDate();
    let mesEsperado = fechaEsperada.getMonth();
    let anioEsperado = fechaEsperada.getFullYear();

    expect(year).equal(anioEsperado);
    expect(month).equal(mesEsperado);
    expect(day).equal(diaEsperado);

});
it('corresponde pagar para un empleado fijo', function () {

    let fechaIncioLaboral = new Date(2019, 4, 8);
    let comprabanteDeFechaHora = new CalculadoraDeFechaDePagoFijo(fechaIncioLaboral);
    let fechaActual=new Date(2019, 4, 31);
    expect(comprabanteDeFechaHora.correspondePagar(fechaActual)).equal(true);

});

it('corresponde pagar para un empleado port hora', function () {

    let fechaIncioLaboral = new Date(2019, 4, 8);
    let comprabanteDeFechaHora = new CalculadoraDeFechaDePagoPorHora(fechaIncioLaboral);
    let fechaActual=new Date(2019, 4, 10);
    expect(comprabanteDeFechaHora.correspondePagar(fechaActual)).equal(true);

});

it('corresponde pagar para un empleado por comision', function () {

    let fechaIncioLaboral = new Date(2019, 4, 30);
    let comprabanteDeFechaHora = new CalculadoraDeFechaDePagoPorComision(fechaIncioLaboral);
    let fechaActual=new Date(2019, 5, 7);
    expect(comprabanteDeFechaHora.correspondePagar(fechaActual)).equal(true);

});

});