var expect = require('chai').expect;
import Cheque from "../Dominio/Entidades/metodoDePago/cheque";
import Deposito from "../Dominio/Entidades/metodoDePago/deposito";
import Efectivo from "../Dominio/Entidades/metodoDePago/efectivo";

describe('metodos de pago', function () {

    it('pago por cheque',  function () {
        let metodoDePago = new Cheque();
        expect(metodoDePago.obtenerPago()).equal("cheque");
    });

    it('pago por deposito',  function () {
        let metodoDePago = new Deposito();
        expect(metodoDePago.obtenerPago()).equal("deposito");
    });

    it('pago por efectivo',  function () {
        let metodoDePago = new Efectivo();
        expect(metodoDePago.obtenerPago()).equal("efectivo");
    });

});